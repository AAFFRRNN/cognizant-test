Feature: Checked Plates

Scenario: Check a valid plate
 	Given I am on the Master Placa
    When I insert plate valid
    Then I checked result valid

Scenario: Check a plate without data
  	Given I am on the Master Placa
  	When I consult plate without data
  	Then I checked result plate without data

Scenario: Check a plate without number plate
  	Given I am on the Master Placa
  	When I insert only number plate valid
  	Then I checked result plate without data

Scenario: Check a plate without letters plate
  	Given I am on the Master Placa
  	When I insert only letter plate valid
  	Then I checked result plate without data

Scenario: Check a invalid plate
  	Given I am on the Master Placa
  	When I insert plate invalid
  	Then I checked result plate invalid