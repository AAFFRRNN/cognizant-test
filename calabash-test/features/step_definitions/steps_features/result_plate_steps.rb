require 'calabash-android/calabash_steps'

Then(/^I checked result valid$/) do
	@checked_situation = page(ResultPlate).result_valid
	@return_consult = page(ResultPlate).return_button
end

Then(/^I checked result plate without data$/) do
	@checked_situation = page(ResultPlate).result_without_data
	@press_confirm = page(ResultPlate).press_confirm
end

Then(/^I checked result plate invalid$/) do
	@checked_situation = page(ResultPlate).result_plate_invalid
	@press_confirm = page(ResultPlate).press_confirm
end