require 'calabash-android/calabash_steps'

When(/^I insert plate valid$/) do
	@text_valid = page(ArrivePlate).insert_plate_valid
	@checked_cofirm = page(ArrivePlate).press_confirm
	@press_consult = page(ArrivePlate).press_consult
end

When(/^I consult plate without data$/) do
	@press_consult = page(ArrivePlate).press_consult
end

When(/^I insert only number plate valid$/) do
	@text_valid = page(ArrivePlate).insert_only_number_plate
	@press_consult = page(ArrivePlate).press_consult
end

When(/^I insert only letter plate valid$/) do
	@text_valid = page(ArrivePlate).insert_only_letter_plate
	@press_consult = page(ArrivePlate).press_consult
end

When(/^I insert plate invalid$/) do
	@text_valid = page(ArrivePlate).insert_plate_invalid
	@press_consult = page(ArrivePlate).press_consult
end