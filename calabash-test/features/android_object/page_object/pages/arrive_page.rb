class ArrivePlate < Calabash::ABase

	def press_consult
		@press_item = page(PressItem).press_id(IdsConstants::ID_CONSULT_PLATES)
	end

	def press_confirm
		@press_item = page(PressItem).press_id(IdsConstants::ID_BUTTON_CONFIRM)
	end

	def press_return
		@press_item = page(PressItem).press_id(IdsConstants::ID_CHECK_PRICES)
	end

	def insert_plate_valid
		@insert_letter = page(InsertText).insert_text(TextsConstants::LETTER_VALID, IdsConstants::ID_LETTERS_PLATES)
		@insert_number = page(InsertText).insert_text(TextsConstants::NUMBER_VALID, IdsConstants::ID_NUMBERS_PLATES)
	end

	def insert_plate_invalid
		@insert_letter = page(InsertText).insert_text(TextsConstants::LETTER_INVALID, IdsConstants::ID_LETTERS_PLATES)
		@insert_number = page(InsertText).insert_text(TextsConstants::NUMBER_INVALID, IdsConstants::ID_NUMBERS_PLATES)
	end

	def insert_only_number_plate
		@press_no_thanks = page(PressItem).press_id(IdsConstants::ID_NO_THANKS)
		@insert_number = page(InsertText).insert_text(TextsConstants::NUMBER_VALID, IdsConstants::ID_NUMBERS_PLATES)
	end

	def insert_only_letter_plate
		@insert_letter = page(InsertText).insert_text(TextsConstants::LETTER_VALID, IdsConstants::ID_LETTERS_PLATES)
	end
end