class ResultPlate < Calabash::ABase

	def result_valid
		@result_plate_valid = page(SeeData) 
		@result_plate_valid.see_text(TextsConstants::RESULT_SITUATION_VALID)
		@result_plate_valid.see_text(TextsConstants::RESULT_MODEL_VALID)
		@result_plate_valid.see_text(TextsConstants::RESULT_COLOR_VALID)
		@result_plate_valid.see_text(TextsConstants::RESULT_YEAR)
		@result_plate_valid.see_text(TextsConstants::RESULT_CHASSI)
	end

	def return_button
		@press_return = page(PressItem).press_id(IdsConstants::ID_BUTTON_RETURN)
	end

	def result_without_data
		@result_plate_valid = page(SeeData).see_text(TextsConstants::RESULT_PLATE_WITHOUT_DATA)
	end

	def result_plate_invalid
		@result_plate_invalid = page(SeeData).see_text(TextsConstants::TEXT_PLATE_INVALID)
	end

	def press_confirm
		@press_item = page(PressItem).press_id(IdsConstants::ID_BUTTON_CONFIRM)
	end
end