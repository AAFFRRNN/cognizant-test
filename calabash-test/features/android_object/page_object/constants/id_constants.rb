require 'calabash-android/abase'

class IdsConstants < Calabash::ABase
	#ID's itens MasterPlaca
	ID_LETTERS_PLATES = "et_placa_letra"
	ID_NUMBERS_PLATES = "et_placa_numero"
	ID_CONSULT_PLATES = "fab_consultar"
	ID_BUTTON_CONFIRM = "button1"
	ID_BUTTON_RETURN = "bt_nova_consulta"
	ID_CHECK_PRICES = "bt_consultar_preco"
	ID_NO_THANKS = "button2"
end