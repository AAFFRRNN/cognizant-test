require 'calabash-android/abase'

class TextsConstants < Calabash::ABase
	#Texts 
	LETTER_VALID = "HHH"
	NUMBER_VALID = "0001"
	LETTER_INVALID = "ABC"
	NUMBER_INVALID = "0000"
	RESULT_SITUATION_VALID = "SEM RESTRIÇÃO"
	RESULT_MODEL_VALID = "FIAT/UNO MILLE FIRE FLEX"
	RESULT_COLOR_VALID = "BRANCA"
	RESULT_YEAR = "2006 / 2007"
	RESULT_CHASSI = "************89485"
	RESULT_PLATE_WITHOUT_DATA = "Por favor, verifique a placa digita."
	TEXT_BUTTON_IMAGE = "marked:'Interstitial close button'"
	TEXT_PLATE_INVALID = "Por favor, verifique se digitou corretamente a placa e tente novamente."
end