require 'calabash-android/abase'
require 'calabash-android/calabash_steps'

class InsertText < Calabash::ABase

	def insert_text(text, identification)
		enter_text("* id:'#{identification}'", text)
	end

	def erase_data(identification)
		clear_text_in("* id:'#{identification}'")
		system("adb shell input keyevent 4")
	end
end