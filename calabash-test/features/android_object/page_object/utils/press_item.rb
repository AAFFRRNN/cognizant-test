require 'calabash-android/abase'

class PressItem < Calabash::ABase

	def press_id(identification)
		identification.split("|").each do |field|
			tap_when_element_exists("* id:'#{field.strip}'")
		end
	end

	def press_marked(identification)
		identification.split("|").each do |field|
			tap_when_element_exists("* marked:'#{field.strip}'")
		end
	end
end