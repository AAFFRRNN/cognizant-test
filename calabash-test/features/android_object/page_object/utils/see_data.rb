require 'calabash-android/abase'

class SeeData < Calabash::ABase

	def see_text(text)
		text.split("|").each do |field|
			wait_for_text(field.strip, timeout: 10)
		end
	end

	def see_id(identification)
		identification.split("|").each do |field|
			wait_for_element_exists("* id:'#{field.strip}'")
		end
	end
end