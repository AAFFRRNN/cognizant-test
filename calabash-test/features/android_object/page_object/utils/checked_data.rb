class CheckedItem < Calabash::ABase

	def advertising_screen
		is_visible = query(QueryConstants::QUERY_BUTTON_IMAGE, 'enabled').first
		if is_visible == true
			@press_item = page(PressItem).press_marked(TextsConstants::TEXT_BUTTON_IMAGE)
		end
	end

	def button_confirm
		is_visible = query(IdsConstants::ID_BUTTON_CONFIRM, 'enabled').first
		if is_visible == true
			@press_item = page(PressItem).press_id(IdsConstants::ID_BUTTON_CONFIRM)
		end
	end
end